import turtle
import random
import datetime

#fac screen
screen = turtle.Screen()
screen.title("Ssssnake")
screen.screensize(1000, 800)
screen.bgcolor("black")  #doar culoare
screen.bgpic("cer.gif")    #imported background image
# screen.delay(100)
#margine
margine = turtle.Turtle()
margine.penup()
margine.color("white")
margine.setposition(-400, -300)  
margine.pendown()
margine.pensize(3)
for latura in range(2):
    margine.forward(800)
    margine.left(90)
    margine.forward(600)
    margine.left(90)
margine.hideturtle()



#fac turtle
cap = turtle.Turtle()
#screen.addshape("ship.gif")  #PROBLEMA : turtle cu image imported nu se roteste
#poc.shape("ship.gif")
cap.color("white")
cap.shape("square")
#cap.shapesize(2, 6, 5)  # inaltime, latime, outline
cap.penup()
cap.speed(0)
speed = 1


#fac food
food = turtle.Turtle()
food.penup()
screen.addshape("2.gif") 
screen.addshape("1.gif")
screen.addshape("3.gif") 
screen.addshape("4.gif")
screen.addshape("5.gif") 
screen.addshape("6.gif")
forme = ["2.gif", "1.gif","3.gif", "4.gif", "5.gif", "6.gif"]
#food.shape("1.gif")
#food.color("purple")
food.goto(random.randint(-390, 390), random.randint(-290, 290))
food.shape(random.choice(forme)) #nu faCE choice de fiecare data
#food.shape("circle")
food.speed(0)

directii = ['u','r','d','l']
directie = 1
print(directie)
print(directii)

def stanga():
    cap.left(90)
    global directie
    global directii
    directie = directie - 1
    if directie < 0: 
        directie = 3
    print(directii[directie])
def dreapta():
    global directie
    global directii
    cap.right(90)
    directie = directie + 1
    if directie > 3: 
        directie = 0
    print(directii[directie])
def viteza():
    global speed
    speed = speed + 1
def incet():
    global speed
    speed = speed - 1

#fac miscari pt turtle
screen.listen()
screen.onkey(stanga, "Left")
screen.onkey(dreapta, "Right")
screen.onkey(viteza, "Up")
screen.onkey(incet, "Down")

corpTot = []  

def get_direction(direction):

    if direction == 0:
        # Capul merge in sus
        return (0, -1)
    elif direction == 2:
        # Capul merge in josq
        return (0, 1)
    elif direction == 1:
        return (-1, 0)
    else:
        # Merge in stanga
        return (1, 0)
# def updateCorp():
#     for obj in corpTot:
#         index = corpTot.index(obj)
#         direction = get_direction(cap.)
#         obj.goto(cap.xcor)
#o misc

while True:
    #nu depasim marginea
    if cap.xcor() > 390 or cap.xcor() < -390:
        cap.right(180)
        if directie == 1:
            directie = 3
        if directie == 3:
            directie = 1 
    if cap.ycor() > 290 or cap.ycor() < -290:
        cap.right(180)
        if directie == 0:
            directie = 2
        if directie == 2:
            directie = 0
    
    
    # print(cap.xcor(), cap.ycor())
    screen.update()
    
    x1 = cap.xcor()
    y1 = cap.ycor()
    cap.forward(speed)
    x2 = cap.xcor()
    y2 = cap.ycor()

    print(datetime.datetime.now())
    print(x2-x1,y2-y1)
    print(speed)
    corp_x, corp_y = get_direction(directie)
    #corpTot.append(cap)
    
    # if cap.xcor() == food.xcor() and cap.ycor() == food.ycor() : #y = cap.ycor() directia???
    #     food.hideturtle()
    # food.setpos(random.randint(10, 400), random.randint(10, 400))

    

    if cap.distance(food) < 20:
        # screen.tracer(8, 25)
        food.shape(random.choice(forme))
        food.goto(random.randint(-390, 390), random.randint(-290, 290))
        screen.update()
        # screen.tracer(1, 0)

        #bucata de corp adaugata
        corp = turtle.Turtle()
        corpTot.append(corp)
        screen.addshape("xx.gif")
        corp.shape("xx.gif")
        corp.penup()
        corp.speed(0)
        
        # for a in corpTot:
        #     print(a.xcor(), a.ycor())
        # print('\n')
    #prima celula dinspre cap merge dupa cap
    # screen.delay(speed)
    if len(corpTot):
        a = get_direction(directie)
        xSign = a[0]
        ySign = a[1]
        distance = speed
       # for i in range(1, len(corpTot)):
        for i in range(len(corpTot) - 1, 0, -1):
            corpCurent = corpTot[i]
            corpDinFata = corpTot[i-1]

            corpCurent.goto(
                corpDinFata.xcor() + 20 * xSign,
                corpDinFata.ycor() + 20 * ySign)
            # corpCurent.goto(
            #     corpDinFata.xcor() + distance * xSign,
            #     corpDinFata.ycor() + distance * ySign
            #     )
        corpTot[0].goto(
            cap.xcor() + 20 * xSign ,
            cap.ycor() + 20 * ySign
            )
        # corpTot[0].goto(
        #     cap.xcor() + distance * xSign,
        #     cap.ycor() + distance * ySign
        #     )
            
        # for i in range(len(corpTot) - 1, 0, -1):
        #     corpCurent = corpTot[i]
        #     corpDinFata = corpTot[i-1]
            
        #     corpTot[0].goto(
        #         corpDinFata.xcor() + 25 * xSign,
        #         corpDinFata.ycor() + 25 * ySign
        #         )
        #     corpCurent.goto(corpDinFata.xcor(), corpDinFata.ycor())

        
        
# #text scor
#     score = turtle.Turtle()
#     score.hideturtle()
#     score.color('white')
#     style = ('Courier', 30, 'italic')
#     score.write("score : %d" %len(corpTot), font=style, align='center')
    

    
        



# def levels():
#     pass
#     #levels
#     # if  len(corpTot) >= 5 :
#     #     speed = speed + 1

    

   

   

turtle.done()


# if __name__ == "__main__":
#     main()

